<?php	
	
	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
		
	if($_SESSION['role'] != '1')
		header('location:index.php');	
	
	include "connection.php";
	
	$states = "";
	$instruments = "";
	$ids = "";
	
	$query = "SELECT State FROM state";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$states = $states.', \''.mysql_result($result, $i, "State").'\'';
	}
	
	$query = "SELECT Instrument FROM instrument";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$instruments = $instruments.', \''.mysql_result($result, $i, "Instrument").'\'';
	}
	
	$states = '('.substr($states, 2).')';
	$instruments = '('.substr($instruments, 2).')';
	
	$query = "SELECT * FROM users WHERE state NOT IN $states OR instrument NOT IN $instruments";
	$result = mysql_query($query, $con);
	
?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Delete Dummy Entries</title>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(function() {
	
});

function del(id, uid) {
	if(confirm("Are you sure?")) {
		$("#row" + id).hide();
		$.ajax({
				url: 'delete_entry.php',
				type: 'POST',
				data: 'id=' + uid,
				success: function(result) {
					alert('Successfully Deleted');
				}
		});
	}
	return false;
	
}

function del_all() {
	if(!confirm("Are you sure?")) {
		return false;
	}
	return true;
}

</script>
</head>

<body background="page_bg.jpg">

	<h2>Dummy Entries</h2>
	<br/><br/>
	
	<form method="POST" action="download_csv.php">
			
		<input type="hidden" name="query" value="<?php echo $query; ?>">
		<input type="hidden" name="loc" value="users">
		<input type="submit" value="Download CSV">
	
	</form>
	
	<form method="POST" action="delete_dummy.php" onsubmit="return del_all();">
			
		<input type="hidden" name="query" value="<?php echo $query; ?>">
		<input type="submit" value="Delete All">
	
	</form>
	
	<table cellspacing="0" width="100%" border="3">
		
		<tr align="center">
			<th>S.No.</th>
			<th>Generated ID</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Age</th>
			<th>Father's/Husband's Name</th>
			<th>Institute</th>
			<th>ID Type</th>
			<th>ID No</th>
			<th>Mobile</th>
			<th>Email</th>
			<th>Address</th>
			<th>City</th>
			<th>Pincode</th>
			<th>State</th>
			<th>Instrument</th>
			<th>Qualification</th>
			<th>Experience</th>
			<th>Delete</th>
		</tr>

<?php
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		
		$ids = $ids.', '.mysql_result($result, $i, "id");
		
?>

		<tr align="center" id="row<?php echo $i + 1; ?>">
			<td><?php echo $i + 1; ?></td>
			<td><?php echo mysql_result($result, $i, "u_id"); ?></td>
			<td><?php echo mysql_result($result, $i, "name"); ?></td>
			<td><?php echo mysql_result($result, $i, "gender"); ?></td>
			<td><?php echo mysql_result($result, $i, "age"); ?></td>
			<td><?php echo mysql_result($result, $i, "fh_name"); ?></td>
			<td><?php echo mysql_result($result, $i, "institute"); ?></td>
			<td><?php echo mysql_result($result, $i, "id_ref"); ?></td>
			<td><?php echo mysql_result($result, $i, "id_no"); ?></td>
			<td><?php echo mysql_result($result, $i, "mobile"); ?></td>
			<td><?php echo mysql_result($result, $i, "email"); ?></td>
			<td><?php echo mysql_result($result, $i, "address"); ?></td>
			<td><?php echo mysql_result($result, $i, "city"); ?></td>
			<td><?php echo mysql_result($result, $i, "pincode"); ?></td>
			<td><?php echo mysql_result($result, $i, "state"); ?></td>
			<td><?php echo mysql_result($result, $i, "instrument"); ?></td>
			<td><?php echo mysql_result($result, $i, "qualification"); ?></td>
			<td><?php echo mysql_result($result, $i, "experience"); ?></td>
			<td><a href="#" onclick="del('<?php echo $i + 1; ?>', '<?php echo mysql_result($result, $i, "u_id"); ?>')">Delete</a></td>
		</tr>

<?php
		
	}
	
	$ids = '('.substr($ids, 2).')';
	
?>

	</table>
</body>
</html>

<?php
	
	mysql_close($con);
		
?>