<?php

  session_start();

  if(!isset($_SESSION['user']))
    header('location:login.php');

  include 'connection.php';

  $query = "SELECT * FROM users WHERE ";

  if(isset($_GET['arr_trn']) && $_GET['arr_trn'] != "na")
    $query = $query."arr_trn='".$_GET['arr_trn']."' AND ";

  if(isset($_GET['arr_stn']) && $_GET['arr_stn'] != "na")
    $query = $query."arr_stn='".$_GET['arr_stn']."' AND ";

  if(isset($_GET['arr_date']) && $_GET['arr_date'] != "na")
    $query = $query."arr_date='".$_GET['arr_date']."' AND ";

  if(isset($_GET['arr_time']) && $_GET['arr_time'] != "na")
    $query = $query."arr_time='".$_GET['arr_time']."' AND ";

  if(isset($_GET['dep_trn']) && $_GET['dep_trn'] != "na")
    $query = $query."dep_trn='".$_GET['dep_trn']."' AND ";

  if(isset($_GET['dep_stn']) && $_GET['dep_stn'] != "na")
    $query = $query."dep_stn='".$_GET['dep_stn']."' AND ";

  if(isset($_GET['dep_date']) && $_GET['dep_date'] != "na")
    $query = $query."dep_date='".$_GET['dep_date']."' AND ";

  if(isset($_GET['dep_time']) && $_GET['dep_time'] != "na")
    $query = $query."dep_time='".$_GET['dep_time']."' AND ";

  $query = substr($query, 0, -5);
  $query .= ";";

  $result = mysql_query($query, $con);

  mysql_close($con);

?>

<html>
<head>
  <title>Train Info</title>
</head>
<body background="page_bg.jpg">
  <h1>Result: </h1>

  <br><br>

  <?php

    if(mysql_num_rows($result) == 0) {

      ?>
      No Results found
      <?php

    } else {

   ?>

  <form method="POST" action="download_csv.php">

    <input type="hidden" name="query" value="<?php echo $query; ?>">
    <input type="hidden" name="loc" value="users">
    <input type="submit" value="Download CSV">

  </form>

  <table cellspacing="0" width="100%" border="3">

  <tr align="center">
    <th>S.No.</th>
    <th>Generated ID</th>
    <th>Name</th>
    <th>Gender</th>
    <th>Age</th>
    <th>Mobile</th>
    <th>Email</th>
    <th>City</th>
    <th>State</th>
    <th>Instrument</th>
    <th>Train Arrival PNR</th>
    <th>Train Arrival Station</th>
    <th>Train Arrival Number</th>
    <th>Train Arrival Date</th>
    <th>Train Arrival Time</th>
    <th>Train Departure PNR</th>
    <th>Train Departure Station</th>
    <th>Train Departure Number</th>
    <th>Train Departure Date</th>
    <th>Train Departure Time</th>
  </tr>

  <?php

    for($i = 0; $i < mysql_num_rows($result); $i ++) {

      ?>

      <tr align="center">
        <td><?php echo $i + 1; ?></td>
        <td><?php echo mysql_result($result, $i, "u_id") ?></td>
        <td><?php echo mysql_result($result, $i, "name"); ?></td>
        <td><?php echo mysql_result($result, $i, "gender"); ?></td>
        <td><?php echo mysql_result($result, $i, "age"); ?></td>
        <td><?php echo mysql_result($result, $i, "mobile"); ?></td>
        <td><?php echo mysql_result($result, $i, "email"); ?></td>
        <td><?php echo mysql_result($result, $i, "city"); ?></td>
        <td><?php echo mysql_result($result, $i, "state"); ?></td>
        <td><?php echo mysql_result($result, $i, "instrument"); ?></td>
        <td><?php echo mysql_result($result, $i, "arr_pnr"); ?></td>
        <td>
          <a href="train_info.php?arr_stn=<?php echo mysql_result($result, $i, "arr_stn"); ?>">
            <?php echo mysql_result($result, $i, "arr_stn"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?arr_trn=<?php echo mysql_result($result, $i, "arr_trn"); ?>">
            <?php echo mysql_result($result, $i, "arr_trn"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?arr_date=<?php echo mysql_result($result, $i, "arr_date"); ?>">
            <?php echo mysql_result($result, $i, "arr_date"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?arr_time=<?php echo mysql_result($result, $i, "arr_time"); ?>">
            <?php echo mysql_result($result, $i, "arr_time"); ?>
          </a>
        </td>
        <td><?php echo mysql_result($result, $i, "dep_pnr"); ?></td>
        <td>
          <a href="train_info.php?dep_stn=<?php echo mysql_result($result, $i, "dep_stn"); ?>">
            <?php echo mysql_result($result, $i, "dep_stn"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?dep_trn=<?php echo mysql_result($result, $i, "dep_trn"); ?>">
            <?php echo mysql_result($result, $i, "dep_trn"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?dep_date=<?php echo mysql_result($result, $i, "dep_date"); ?>">
            <?php echo mysql_result($result, $i, "dep_date"); ?>
          </a>
        </td>
        <td>
          <a href="train_info.php?dep_time=<?php echo mysql_result($result, $i, "dep_time"); ?>">
            <?php echo mysql_result($result, $i, "dep_time"); ?>
          </a>
        </td>
      </tr>

      <?php

    }

  ?>

  </table>

  <br><br>

  Total no. of result rows: <?php echo mysql_num_rows($result); ?>

  <?php

    }

  ?>

</body>
</html>
