<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	if($_SESSION['role'] != '1')
		header('location:index.php');

	include 'connection.php';

	$offset = 0;

	$type = $_POST['type'];

	if($type != 'delete') {

		$query = "SELECT State, Code FROM state;";
		$result = mysql_query($query, $con);

		$states = array();
		$instruments = array();

		$duplicates = array();
		$duplicates_num = 0;

		$start_log = 0;
		$log_id = 0;
		date_default_timezone_set('Asia/Kolkata');

		$count_insert = 0;

		for($count = 0; $count < mysql_num_rows($result); $count ++) {
			$state = strtolower(mysql_result($result, $count, "State"));
			$code = mysql_result($result, $count, "Code");

			$states[$state] = $code;
		}

		$query = "SELECT Instrument, Code FROM instrument;";
		$result = mysql_query($query, $con);

		for($count = 0; $count < mysql_num_rows($result); $count ++) {
			$instrument = strtolower(mysql_result($result, $count, "Instrument"));
			$code = mysql_result($result, $count, "Code");

			$instruments[$instrument] = $code;
		}
		$instruments['dummy'] = 'DM';

		$query = "SELECT state, id FROM state";
		$result = mysql_query($query, $con);
		$arr_state_ids = array();
		for($k = 0; $k < mysql_num_rows($result); $k ++) {
			$arr_state_ids[strtolower(mysql_result($result, $k, "state"))] = transform_id(mysql_result($result, $k, "id"));
		}

		$query = "SELECT u_id FROM users WHERE id = (SELECT MAX(id) FROM users);";
		$result = mysql_query($query, $con);
		$old_uid = "0";
		if(mysql_num_rows($result) == 1)
			$old_uid = mysql_result($result, 0, "u_id");
		if($old_uid != "0")
			$highest_count = intval(substr($old_uid, -5));
	}

	if(isset($_POST['offset']))
		$offset = intval($_POST['offset']);

	if(isset($_POST['offset']))
		$form_of_entry = $_POST['entry'];

	if(is_uploaded_file($_FILES['csvFile']['tmp_name'])) {

		$name = $_FILES['csvFile']['name'];
		$extension = explode('.', $name)[1];

		if($extension != 'csv')
			echo "Please Upload CSV file only.";
		else {

			ini_set('auto_detect_line_endings', true);
			$handle = fopen($_FILES['csvFile']['tmp_name'], "r");

			$i = 0;
			$failed = array();

			while (($line = fgetcsv($handle)) !== FALSE) {

				if($i < $offset - 1) {
					$i += 1;
					continue;
				}

				if($type == 'insert') {
					$name = clean($line[0]);
					$gender = clean($line[1]);
					$age = clean($line[2]);
					$fh_name = clean($line[3]);
					$institute = clean($line[4]);
					$id_ref = clean($line[5]);
					$id_no = clean($line[6]);
					$mobile = clean($line[7]);
					$email = clean($line[8]);
					$address = clean($line[9]);
					$city = clean($line[10]);
					$pincode = clean($line[11]);
					$state = clean(strtolower($line[12]));
					$instrument = clean(strtolower($line[13]));
					$qualification = clean($line[14]);
					$experience = clean($line[15]);
					$remarks = clean($line[16]);
					$volname = clean($line[17]);
					$volmob = clean($line[18]);
					$volemail = clean($line[19]);
					$mode_of_transport = clean($line[20]);
					$arr_pnr = clean($line[21]);
					$arr_trn = clean($line[22]);
					$arr_stn = clean($line[23]);
					$arr_date = date('Y-m-d', strtotime($line[24]));
					$arr_time = clean($line[25]);
					$dep_pnr = clean($line[26]);
					$dep_trn = clean($line[27]);
					$dep_stn = clean($line[28]);
					$dep_date = date('Y-m-d', strtotime($line[29]));
					$dep_time = clean($line[30]);
				} else if($type == 'update') {
					$uid = clean($line[0]);
					$name = clean($line[1]);
					$gender = clean($line[2]);
					$age = clean($line[3]);
					$fh_name = clean($line[4]);
					$institute = clean($line[5]);
					$id_ref = clean($line[6]);
					$id_no = clean($line[7]);
					$mobile = clean($line[8]);
					$email = clean($line[9]);
					$address = clean($line[10]);
					$city = clean($line[11]);
					$pincode = clean($line[12]);
					$state = clean(strtolower($line[13]));
					$instrument = clean(strtolower($line[14]));
					$qualification = clean($line[15]);
					$experience = clean($line[16]);
					$remarks = clean($line[17]);
					$volname = clean($line[18]);
					$volmob = clean($line[19]);
					$volemail = clean($line[20]);
					$mode_of_transport = clean($line[21]);
					$arr_pnr = clean($line[22]);
					$arr_trn = clean($line[23]);
					$arr_stn = clean($line[24]);
					$arr_date = date('Y-m-d', strtotime($line[25]));
					$dep_time = clean($line[26]);
					$dep_pnr = clean($line[27]);
					$dep_trn = clean($line[28]);
					$dep_stn = clean($line[29]);
					$dep_date = date('Y-m-d', strtotime($line[30]));
					$dep_time = clean($line[31]);
				} else if($type == 'delete') {
					$uid = clean($line[0]);
				}

				if($type != 'delete') {
					if($instrument == '') {
						$instrument = 'dummy';
					}

					$id_str = getString($i - $offset + 1);
					$state_code = $states[$state];
					$state_id = $arr_state_ids[$state];
					$instrument_code = $instruments[$instrument];
				}

				if($type == 'insert') {
					$check = 0;
					$query = "SELECT * FROM users WHERE name LIKE '%$name%' AND name <> ''";
					$result = mysql_query($query, $con);
					if(mysql_num_rows($result) > 0) {
						$query = "SELECT * FROM users
									WHERE name='$name' AND (
									(fh_name='$fh_name' AND fh_name <> '')
									OR (email = '$email' AND email <> '')
									OR (id_no='$id_no' AND id_no <> '')
									OR (mobile='$mobile' AND mobile <> ''));";
						$result = mysql_query($query, $con) OR die(mysql_error().$query);
						if(mysql_num_rows($result) > 0) {
							$check = 1;
						}
					}

					if($check == 0) {

						$uniq_ctr = getString($highest_count + 1);
						$uid = $state_code.$form_of_entry.'_'.$instrument_code.$state_id.$uniq_ctr;

						$query = "INSERT INTO users
								(u_id, name, gender, age, fh_name, institute,
								id_ref, id_no, mobile, email, address,
								city, pincode, state, instrument, qualification, experience, remarks, volname, volmob, volemail, mode_transport,
								arr_pnr, arr_stn, arr_trn, arr_date, arr_time, dep_pnr, dep_stn,
								dep_trn, dep_date, dep_time) VALUES
								('$uid', '$name', '$gender', '$age', '$fh_name', '$institute', '$id_ref',
								'$id_no', '$mobile', '$email', '$address', '$city', '$pincode',
								'$state', '$instrument', '$qualification', '$experience', '$remarks', '$volname', '$volmob', '$volemail', '$mode_of_transport',
								'$arr_pnr', '$arr_stn', '$arr_trn', '$arr_date', '$arr_time',
								'$dep_pnr', '$dep_stn', '$dep_trn', '$dep_date', '$dep_time');";
						$result = mysql_query($query, $con) or die(mysql_error());
						$highest_count++;

						if($count_insert == 0) {

							$start_log = intval(mysql_insert_id());
							$date = date('Y-m-d', time());
							$time = date('H:i:s', time());
							$query = "INSERT INTO logs (date, time, type, start, end) VALUES
										('$date', '$time', '$form_of_entry', '$start_log', '$start_log')";
							$result = mysql_query($query, $con);
							$log_id = mysql_insert_id();

						} else {
							$end_log = $start_log + $count_insert;
							$query = "UPDATE logs SET end = '$end_log' WHERE id = '$log_id'";
							$result = mysql_query($query, $con);
						}

						$count_insert++;

					} else {

						$duplicates[$duplicates_num]["name"] = $name;
						$duplicates[$duplicates_num]["gender"] = $gender;
						$duplicates[$duplicates_num]["age"] = $age;
						$duplicates[$duplicates_num]["fh_name"] = $fh_name;
						$duplicates[$duplicates_num]["institute"] = $institute;
						$duplicates[$duplicates_num]["id_ref"] = $id_ref;
						$duplicates[$duplicates_num]["id_no"] = $id_no;
						$duplicates[$duplicates_num]["mobile"] = $mobile;
						$duplicates[$duplicates_num]["email"] = $email;
						$duplicates[$duplicates_num]["address"] = $address;
						$duplicates[$duplicates_num]["city"] = $city;
						$duplicates[$duplicates_num]["pincode"] = $pincode;
						$duplicates[$duplicates_num]["state"] = $state;
						$duplicates[$duplicates_num]["instrument"] = $instrument;
						$duplicates[$duplicates_num]["qualification"] = $qualification;
						$duplicates[$duplicates_num]["experience"] = $experience;

						$duplicates_num ++;

					}
				} else if($type == 'update') {
					$new_state_code = $states[$state];
					$new_instrument_code = $instruments[$instrument];
					$new_state_id = $arr_state_ids[$state];
					$old_form_entry = substr(explode('_', $uid)[0], -2);
					$new_uid = $new_state_code.$old_form_entry.'_'.$new_instrument_code.$new_state_id.substr($uid, -5);
					$query = "UPDATE users SET u_id = '$new_uid', name='$name', gender='$gender', age='$age',
								fh_name='$fh_name', institute='$institute', id_ref='$id_ref',
								id_no='$id_no', mobile='$mobile', email='$email',
								address='$address', city='$city', pincode='$pincode',
								state='$state', instrument='$instrument', qualification='$qualification',
								experience='$experience', remarks='$remarks', volname='$volname', volmob='$volmob', volemail='$volemail', mode_transport='$mode_of_transport',
								arr_pnr='$arr_pnr', arr_stn='$arr_stn', arr_trn='$arr_trn', arr_date='$arr_date', arr_time='$arr_time',
								dep_pnr='$dep_pnr', dep_stn='$dep_stn', dep_trn='$dep_trn', dep_date='$dep_date', dep_time='$dep_time'
								WHERE u_id='$uid'";
					$result = mysql_query($query, $con) or die(mysql_error());
				} else if($type == 'delete') {
					$query = "SELECT * FROM users WHERE u_id = '$uid'";
					$num_failed = mysql_num_rows(mysql_query($query, $con));
					if($num_failed == 1) {
						$query = "DELETE FROM users WHERE u_id = '$uid';";
						$result = mysql_query($query, $con);
					} else {
						array_push($failed, $uid);
					}
				}

				$i ++;

			}

		?>

			<html>
			<head>
			<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
			<title>Finished Uploading</title>
			</head>
			<body background="page_bg.jpg">
			<b><?php
				if($type == 'delete') {
					echo $i - count($failed) - 1;
					?></b> records successfully deleted<br><br>
					<p><?php echo count($failed); ?> records could not be found: </p><br><br>
					<table width="50%" border="3" cellspacing="0">
						<tr>
							<th>SNo</th>
							<th>Name</th>
						</tr>
					<?php
						for($del = 0; $del < count($failed); $del ++) {
							?>
							<tr align="center">
								<td><?php echo $del + 1; ?></td>
								<td><?php echo $failed[$del]; ?></td>
							</tr>
							<?php
						}
					?>
				</table>
				<?php
				} else {
			echo ($i - $offset - $duplicates_num + 1); ?></b> rows of CSV successfully imported<br><br>
			<p><?php echo $duplicates_num; ?> rows found to be duplicate: </p>
				<table width="100%" border="3" cellspacing="0">
					<tr>
						<th>SNo</th>
						<th>Name</th>
						<th>Age</th>
						<th>Gender</th>
						<th>Father'/Husband's Name</th>
						<th>Institute</th>
						<th>ID Card</th>
						<th>ID Vard No.</th>
						<th>Mobile</th>
						<th>Email</th>
						<th>Address</th>
						<th>City</th>
						<th>Pincode</th>
						<th>State</th>
						<th>Instruments</th>
						<th>Qualification</th>
						<th>Experience</th>
					</tr>
				<?php

					for($di = 0; $di < $duplicates_num; $di ++) {

						?>

						<tr align="center">

							<td><?php echo $di + 1; ?></td>
							<td><?php echo $duplicates[$di]["name"]; ?></td>
							<td><?php echo $duplicates[$di]["age"]; ?></td>
							<td><?php echo $duplicates[$di]["gender"]; ?></td>
							<td><?php echo $duplicates[$di]["fh_name"]; ?></td>
							<td><?php echo $duplicates[$di]["institute"]; ?></td>
							<td><?php echo $duplicates[$di]["id_ref"]; ?></td>
							<td><?php echo $duplicates[$di]["id_no"]; ?></td>
							<td><?php echo $duplicates[$di]["mobile"]; ?></td>
							<td><?php echo $duplicates[$di]["email"]; ?></td>
							<td><?php echo $duplicates[$di]["address"]; ?></td>
							<td><?php echo $duplicates[$di]["city"]; ?></td>
							<td><?php echo $duplicates[$di]["pincode"]; ?></td>
							<td><?php echo $duplicates[$di]["state"]; ?></td>
							<td><?php echo $duplicates[$di]["instrument"]; ?></td>
							<td><?php echo $duplicates[$di]["qualification"]; ?></td>
							<td><?php echo $duplicates[$di]["experience"]; ?></td>

						</tr>

						<?php

					}

				?>
			</table>
			<?php } ?>
			</body>
			</html>

			<?php

		}

	} else {

		echo "There was some problem uploading";

	}

	mysql_close($con);

	function getString($no) {

		$str_no = strval($no);

		if(strlen($str_no) == 1)
			$str_no = '0000'.$str_no;
		else if(strlen($str_no) == 2)
			$str_no = '000'.$str_no;
		else if(strlen($str_no) == 3)
			$str_no = '00'.$str_no;
		else if(strlen($str_no) == 4)
			$str_no = '0'.$str_no;

		return $str_no;

	}

	function clean($str) {

		$str = str_replace("'", "", $str);
		$str = str_replace("\\", "\\\\", $str);
		$str = trim($str);

		return $str;
	}

	function transform_id($id) {
		$s_id = strval($id);
		if(strlen($s_id) == 1)
			$s_id = '0'.$s_id;
		return $s_id;
	}

?>
