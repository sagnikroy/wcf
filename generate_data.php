<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	include 'connection.php';
	if(!isset($_POST['state']) || !isset($_POST['instrument']))
		die("Please select state/instrument");
	$state = $_POST['state'];
	$instrument = $_POST['instrument'];
	$header;

	$total_count = 0;

	if($state == 'all' && $instrument == 'all') {

		$query = "SELECT * FROM users ORDER BY instrument, state, name;";

	} else if ($instrument == 'all') {

		$states = '\''.strtolower(implode('\',\'', $_POST['state'])).'\'';
		$query = "SELECT * FROM users WHERE state IN ($states) ORDER BY instrument, name;";

	} else if($state == 'all') {

		$instruments = '\''.strtolower(implode('\',\'', $_POST['instrument'])).'\'';
		$query = "SELECT * FROM users WHERE instrument IN ($instruments) ORDER BY state, name";

	} else {

		$states = '\''.strtolower(implode('\',\'', $_POST['state'])).'\'';
		$instruments = '\''.strtolower(implode('\',\'', $_POST['instrument'])).'\'';

		$query = "SELECT * FROM users WHERE state IN ($states) AND instrument IN ($instruments) ORDER BY instrument, state, name;";
	}

	$result = mysql_query($query, $con) or die(mysql_error());

	mysql_close($con);

?>

<html>

	<head>

		<title>Custom Report</title>

	</head>

	<body background="page_bg.jpg">

		<h1>Result: </h1>

		<br><br>

		<form method="POST" action="download_csv.php">

			<input type="hidden" name="query" value="<?php echo $query; ?>">
			<input type="hidden" name="loc" value="users">
			<input type="submit" value="Download CSV">

		</form>

		<form method="POST" action="send_mail.php">

			<input type="hidden" name="query" value="<?php echo $query; ?>">
			<input type="submit" value="Send Email To All">

		</form>

		</br><br>

		<table cellspacing="0" width="100%" border="3">

		<tr align="center">
			<th>S.No.</th>
			<th>Generated ID</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Age</th>
			<th>Father's/Husband's Name</th>
			<th>Institute</th>
			<th>ID Type</th>
			<th>ID No</th>
			<th>Mobile</th>
			<th>Email</th>
			<th>Address</th>
			<th>City</th>
			<th>Pincode</th>
			<th>State</th>
			<th>Instrument</th>
			<th>Qualification</th>
			<th>Experience</th>
			<th>Volunteer Name</th>
			<th>Volunteer Mobile</th>
			<th>Volunteer Email</th>
			<th>Mode of Transport</th>
			<th>Arrival PNR</th>
			<th>Arrival Station</th>
			<th>Arrival Train</th>
			<th>Arrival Date</th>
			<th>Arrival Time</th>
		</tr>

		<?php

			for($i = 0; $i < mysql_num_rows($result); $i ++) {

				?>

				<tr align="center">
					<td><?php echo $i + 1; ?></td>
					<td><?php echo mysql_result($result, $i, "u_id") ?></td>
					<td><?php echo mysql_result($result, $i, "name"); ?></td>
					<td><?php echo mysql_result($result, $i, "gender"); ?></td>
					<td><?php echo mysql_result($result, $i, "age"); ?></td>
					<td><?php echo mysql_result($result, $i, "fh_name"); ?></td>
					<td><?php echo mysql_result($result, $i, "institute"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_ref"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_no"); ?></td>
					<td><?php echo mysql_result($result, $i, "mobile"); ?></td>
					<td><?php echo mysql_result($result, $i, "email"); ?></td>
					<td><?php echo mysql_result($result, $i, "address"); ?></td>
					<td><?php echo mysql_result($result, $i, "city"); ?></td>
					<td><?php echo mysql_result($result, $i, "pincode"); ?></td>
					<td><?php echo mysql_result($result, $i, "state"); ?></td>
					<td><?php echo mysql_result($result, $i, "instrument"); ?></td>
					<td><?php echo mysql_result($result, $i, "qualification"); ?></td>
					<td><?php echo mysql_result($result, $i, "experience"); ?></td>
					<td><?php echo mysql_result($result, $i, "volname"); ?></td>
					<td><?php echo mysql_result($result, $i, "volmob"); ?></td>
					<td><?php echo mysql_result($result, $i, "volemail"); ?></td>
					<td><?php echo mysql_result($result, $i, "mode_transport"); ?></td>
					<td><?php echo mysql_result($result, $i, "arr_pnr"); ?></td>
					<td><?php echo mysql_result($result, $i, "arr_stn"); ?></td>
					<td><?php echo mysql_result($result, $i, "arr_trn"); ?></td>
					<td><?php echo mysql_result($result, $i, "arr_date"); ?></td>
					<td><?php echo mysql_result($result, $i, "arr_time"); ?></td>
				</tr>

				<?php

			}

		?>

		</table>

		<br><br>

		Total no. of result rows: <?php echo mysql_num_rows($result); ?>

	</body>

</html>
