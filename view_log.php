<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	if($_SESSION['role'] != '1')
		header('location:index.php');
	
	include 'connection.php';
	
	$start = $_GET['start'];
	$end = $_GET['end'];
	
	$query = "SELECT * FROM users WHERE id >= '$start' AND id <= '$end'";
	$result = mysql_query($query, $con);
	
	if(mysql_num_rows($result) == 0)
		die("No records found for this log");
	
?>

<html>
<head>
<title>View Log Data</title>
</head>
<body background="page_bg.jpg">
<h1>Log Data</h1>

<table width="100%" border="3" cellspacing="0">

	<tr align="center">
		<th>S.No.</th>
		<th>Generated ID</th>
		<th>Name</th>
		<th>Gender</th>
		<th>Age</th>
		<th>Father's/Husband's Name</th>
		<th>Institute</th>
		<th>ID Type</th>
		<th>ID No</th>
		<th>Mobile</th>
		<th>Email</th>
		<th>Address</th>
		<th>City</th>
		<th>Pincode</th>
		<th>State</th>
		<th>Instrument</th>
		<th>Qualification</th>
		<th>Experience</th>
	</tr>
	
	<?php
		
			for($i = 0; $i < mysql_num_rows($result); $i ++) {
				
				?>
				
				<tr align="center">
					<td><?php echo $i + 1; ?></td>
					<td><?php echo mysql_result($result, $i, "u_id") ?></td>
					<td><?php echo mysql_result($result, $i, "name"); ?></td>
					<td><?php echo mysql_result($result, $i, "gender"); ?></td>
					<td><?php echo mysql_result($result, $i, "age"); ?></td>
					<td><?php echo mysql_result($result, $i, "fh_name"); ?></td>
					<td><?php echo mysql_result($result, $i, "institute"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_ref"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_no"); ?></td>
					<td><?php echo mysql_result($result, $i, "mobile"); ?></td>
					<td><?php echo mysql_result($result, $i, "email"); ?></td>
					<td><?php echo mysql_result($result, $i, "address"); ?></td>
					<td><?php echo mysql_result($result, $i, "city"); ?></td>
					<td><?php echo mysql_result($result, $i, "pincode"); ?></td>
					<td><?php echo mysql_result($result, $i, "state"); ?></td>
					<td><?php echo mysql_result($result, $i, "instrument"); ?></td>
					<td><?php echo mysql_result($result, $i, "qualification"); ?></td>
					<td><?php echo mysql_result($result, $i, "experience"); ?></td>
				</tr>
				
				<?php
				
			}
			
		?>

</table>


</body>
</html>

<?php
	
	mysql_close($con);

?>