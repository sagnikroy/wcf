<?php

  $id = $_GET['id'];

  include 'connection.php';

  $query = "SELECT * FROM users WHERE id = '$id'";
  $result = mysql_query($query, $con);

  mysql_close($con);

  $uid = mysql_result($result, 0, "u_id");
  $name = strtoupper(mysql_result($result, 0, "name"));
  $gender = mysql_result($result, 0, "gender");
  $age = mysql_result($result, 0, "age");
  $fh_name = strtoupper(mysql_result($result, 0, "fh_name"));
  $institute = strtoupper(mysql_result($result, 0, "institute"));
  $id_ref = strtoupper(mysql_result($result, 0, "id_ref"));
  $id_no = mysql_result($result, 0, "id_no");
  $mobile = mysql_result($result, 0, "mobile");
  $email = mysql_result($result, 0, "email");
  $address = mysql_result($result, 0, "address");
  $city = strtoupper(mysql_result($result, 0, "city"));
  $pincode = mysql_result($result, 0, "pincode");
  $country = "INDIA";
  $state = strtoupper(mysql_result($result, 0, "state"));
  $instrument = strtoupper(mysql_result($result, 0, "instrument"));
  $qualification = strtoupper(mysql_result($result, 0, "qualification"));
  $experience = strtoupper(mysql_result($result, 0, "experience"));
  $volunteer_name = mysql_result($result, 0, "volname");
  $volunteer_mobile = mysql_result($result, 0, "volmob");
  $volunteer_email = mysql_result($result, 0, "volemail");
  if(strlen($address) > 55)
    $address = substr($address, 0, 56).'<br/>'.substr($address, 56);

?>

<html>
<head>
  <title>Registration Form</title>
  <style>
    body {
      margin: 3px;
      border: 3px solid #000;
      padding: 20px;
      font-size: 11px;
    }
    #photo {
      border: 1px solid #000;
      width: 180px;
      font-size: 8px;
      height: 20px;
      position:fixed;
      margin-right: 30px;
      right: 10px;
      padding-top:160px;
      text-align: center;
      font-weight: bolder;
      padding-bottom:5px;
    }
  </style>
</head>

<body>
  <div style="float:right;">
    <span><img src="WCF_Logo.png" width="100" height="100" style="margin-top:5px;"/></span>
  </div>
  <br/>
  <center style="margin-left:50px;">

    <p>
      <h1 style="margin-left:20px;">World Culture Festival</h1>
      <span>11, 12, 13 March, 2016, New Delhi</span><br/>
      <span>
        <strong>Web:</strong> www.worldculturalfestival.in
        <strong>Email:</strong> music@worldculturalfestival.in
      </span>
    </p>
  </center>
  <center>
    <b><i>
      Meditation makes life musical and music can lead to a deep sense
      of inner peace ~Sri Sri Ravi Shankar
    </i></b>
  </center>
  <center>
    <p style="font-size:16px;">
      <b><u>MUSICIAN APPLICATION FORM</u></b><br/>
      <u style="margin-top:5px;">(PLEASE FILL THE FORM IN BLOCK/CAPITAL LETTERS)</u>
    </p>
  </center>
  <div id="photo">
    (Attach passport size color photo taken within last 6 months.)
  </div>
  <p>
    <b>Full Name:</b> <u><?php echo $name; ?></u><br/><br/>
    <b>Father's/Husband's Name:</b> <u><?php echo $fh_name; ?></u><br/><br/>
    <span style="float:left;"><b>Sex:</b> <u><?php echo $gender; ?></u></span>
    <span style="margin-left:80px;"><b>Age:</b> <u><?php echo $age; ?></u></span><br/><br/>
    <b>Name of College/Institute:</b> <u><?php echo $institute; ?></u><br/><br/>
    <b>ID Card No.:</b><?php echo (empty($id_no)) ? '______________________' : '<u>'.$id_no.'</u>'; ?>
    <?php echo (empty($id_ref)) ? '(Aadhar Card / Voter ID <br/>/ Passport / Driving License / Student ID with Photo / ID issued by Govt.)' : '('.$id_ref.')'; ?>
  </p>
  <p>
    <b>Address:</b> <u><?php echo $address; ?></u><br/><br/>
    <span style="margin-right:40px;"><b>Pin Code:</b> <u><?php echo $pincode; ?></u></span>
    <span style="margin-right:40px;"><b>City:</b> <u><?php echo $city; ?></u></span>
    <span style="margin-right:40px;"><b>State:</b> <u><?php echo $state; ?></u></span><br/><br/>
    <span><b>Country: </b><u><?php echo $country; ?></u></span>
  </p>
  <p>
    <span style="margin-right:50px;"><b>Mobile No.:</b> <u><?php echo $mobile; ?></u></span>
    <span><b>Email Address: </b><u><?php echo $email; ?></u></span>
  </p>
  <p>
    <b>Type of Musical Instrument: </b><?php echo (empty($instrument)) ? '______________________ (*No Electrical Instruments)' : '<u>'.$instrument.'</u>'; ?>
  </p>
  <p>
    <b>Qualification in Musical Instrument: </b><?php echo (empty($qualification)) ? '______________________ (Diploma / Degree / Others)' : '<u>'.$qualification.'</u>'; ?>
  </p>
  <p>
    <b>Experience in Musical Instrument: </b><?php echo (empty($experience)) ? '______________________ (6-12 Months / 1-2 Yrs / 3-4 Yrs <br/>/ 4 Yrs & Above)' : '<u>'.$experience.'</u>'; ?>
  </p>
  <br/><br/>
  <hr style="border-top:dotted 1px;">
  <p>
    <b>Declaration: (For participants below the age of 18 years, declaration needs
      to be duly signed by Parent/Guardian)<span style="float:right">*Strike out wherever applicable</span></b>
    <ol style="font-size:10px">
      <li>
        I hereby declare that (I / my child/ward)* will be participating in the
        World Cultural Festival organised by Vyakti Vikas Kendra, India on
        voluntary basis, with my complete knowledge and at my own risk.
      </li>
      <li>
        I take full responsibility for (my / my child/ward's)* participation in
        this event. I release Vyakti Vikas Kendra India, all organisers,
        coordinators and assistants of this event from damages arising due to
        negligence of any person or whatsoever and waive all rights to compensation.
        I declare that I am physically and mentally fit and able to participate
        in this event.
      </li>
      <li>
        I bear complete knowledge that all the music and notations that will be
        played by (me / my child/ward)* are copyrighted under the applicable laws
        and all the rights for the music and notations are reserved and unauthorised
        copying and use of the same in any form is strictly prohibited.
      </li>
    </ol>
    <br/><br/>
    Date: ____________________
    Place: _____________________
    <span style="float:right;">Signature: ____________________</span>
  </p>
  <br/><br/>
  <hr style="border-top:dotted 1px;">
  <strong><u>For Official Use Only</u></strong><br/><br/>
  <b>Received by (Name of Teacher/Volunteer): </b><?php echo (empty($volunteer_name)) ? '_______________________________________' : $volunteer_name; ?><br/><br/>
  <b>Mobile No.: </b><?php echo (empty($volunteer_mobile)) ? '_______________________________________' : $volunteer_mobile; ?>
  <b>Email: </b><?php echo (empty($volunteer_email)) ? '_______________________________________' : $volunteer_email; ?><br/>
  <span style="margin-top:30px;float:right;">
    <strong>Generated ID: </strong><?php echo $uid; ?>
  </span>
</body>
</html>
