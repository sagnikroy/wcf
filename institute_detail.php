<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	include 'connection.php';

	if(!isset($_GET['param']))
		die("Broken link");

	$param = urldecode($_GET['param']);

	$param_array = explode(":", $param);

	$institute = $param_array[0];
	$state = $param_array[1];
	$city = $param_array[2];

	$query = "SELECT * FROM users
		WHERE institute = '$institute' AND state = '$state' AND city = '$city';";

	$result = mysql_query($query, $con) or die(mysql_error());

	mysql_close($con);

?>

<html>

	<head>

		<title>Data</title>

	</head>

	<body background="page_bg.jpg">

		<h1>Users From <?php echo ucwords(strtolower($institute)); ?</h1>

		<br><br>

		<form method="POST" action="download_csv.php">

			<input type="hidden" name="query" value="<?php echo $query; ?>">
			<input type="hidden" name="loc" value="users">
			<input type="submit" value="Download CSV">

		</form>

		</br><br>

		<table cellspacing="0" width="100%" border="3">

		<tr align="center">
			<th>S.No.</th>
			<th>Generated ID</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Age</th>
			<th>Father's/Husband's Name</th>
			<th>Institute</th>
			<th>ID Type</th>
			<th>ID No</th>
			<th>Mobile</th>
			<th>Email</th>
			<th>Address</th>
			<th>City</th>
			<th>Pincode</th>
			<th>State</th>
			<th>Instrument</th>
			<th>Qualification</th>
			<th>Experience</th>
		</tr>

		<?php

			for($i = 0; $i < mysql_num_rows($result); $i ++) {

				?>

				<tr align="center">
					<td><?php echo $i + 1; ?></td>
					<td><?php echo mysql_result($result, $i, "u_id") ?></td>
					<td><?php echo mysql_result($result, $i, "name"); ?></td>
					<td><?php echo mysql_result($result, $i, "gender"); ?></td>
					<td><?php echo mysql_result($result, $i, "age"); ?></td>
					<td><?php echo mysql_result($result, $i, "fh_name"); ?></td>
					<td><?php echo mysql_result($result, $i, "institute"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_ref"); ?></td>
					<td><?php echo mysql_result($result, $i, "id_no"); ?></td>
					<td><?php echo mysql_result($result, $i, "mobile"); ?></td>
					<td><?php echo mysql_result($result, $i, "email"); ?></td>
					<td><?php echo mysql_result($result, $i, "address"); ?></td>
					<td><?php echo mysql_result($result, $i, "city"); ?></td>
					<td><?php echo mysql_result($result, $i, "pincode"); ?></td>
					<td><?php echo mysql_result($result, $i, "state"); ?></td>
					<td><?php echo mysql_result($result, $i, "instrument"); ?></td>
					<td><?php echo mysql_result($result, $i, "qualification"); ?></td>
					<td><?php echo mysql_result($result, $i, "experience"); ?></td>
				</tr>

				<?php

			}

		?>

		</table>

		<br><br>

		Total no. of result rows: <?php echo mysql_num_rows($result); ?>

	</body>

</html>
