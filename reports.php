<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	include 'connection.php';

	$instruments = array();
	$states = array();
	$states_exclusion_list = "('andhra pradesh','arunachal pradesh','assam','chandigarh',
															'manipur','meghalaya','mizoram','nagaland','puducherry',
															'sikkim','tripura')";

	$query = "SELECT Instrument FROM instrument ORDER BY Instrument;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$instruments[$i] = mysql_result($result, $i, "Instrument");

	}

	$query = "SELECT State FROM state WHERE state NOT IN $states_exclusion_list ORDER BY State;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$states[$i] = mysql_result($result, $i, "State");

	}

	mysql_close($con);

?>

<html>

	<head>
	<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />

		<title>Custom Reports</title>
		<script src="jquery.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {

				delhi_flag = false;
				rest_flag = false;

				$('#state_none').click(function() {
					$('#state_none').prop('checked', true);
					$('#state_all').prop('checked', false);
					$('.state').prop('checked', false);
				});

				$('#state_all').click(function() {
					$('#state_all').prop('checked', true);
					$('#state_none').prop('checked', false);
					$('.state').prop('checked', false);
				});

				$('#instrument_none').click(function() {
					$('#instrument_none').prop('checked', true);
					$('#instrument_all').prop('checked', false);
					$('.instrument').prop('checked', false);
				});

				$('#instrument_all').click(function() {
					$('#instrument_all').prop('checked', true);
					$('#instrument_none').prop('checked', false);
					$('.instrument').prop('checked', false);
				});

				$('#delhi').click(function() {
					if(!delhi_flag) {
						for(var i = 3; i <= 7; i++)
							$('input[name="state[' + i + ']"]').prop('checked', true);
						for(var i = 18; i <= 24; i++)
							$('input[name="state[' + i + ']"]').prop('checked', true);
						$('#state_all').prop('checked', false);
						$('#state_none').prop('checked', false);
					} else {
						$('.state').prop('checked', false);
						$('#state_all').prop('checked', true);
					}
					delhi_flag = !delhi_flag;
				});

				$('#rest').click(function() {
					if(!rest_flag) {
						for(var i = 1; i <= 2; i++)
							$('input[name="state[' + i + ']"]').prop('checked', true);
						for(var i = 8; i <= 17; i++)
							$('input[name="state[' + i + ']"]').prop('checked', true);
						for(var i = 25; i <= 33; i++)
							$('input[name="state[' + i + ']"]').prop('checked', true);
						$('#state_all').prop('checked', false);
						$('#state_none').prop('checked', false);
					} else {
						$('.state').prop('checked', false);
						$('#state_all').prop('checked', true);
					}
					rest_flag = !rest_flag;
				});

				$('#submit').click(function() {
					if($('#instrument_none').is(':checked') &&
						$('#state_none').is(':checked')) {
							alert('Can\'t select none for both!');
							return false;
						}
				});

			});

			function submit_form() {

				var state = document.getElementById('state').value;
				var instrument = document.getElementById('instrument').value;

				if(state == instrument) {
					alert('Can\'t select all or none for both!');
					return false;
				}

				return true;
			}

			function check_state() {
				if($('#state_none').is(':checked'))
					$('#state_none').prop('checked', false);
				if($('#state_all').is(':checked'))
					$('#state_all').prop('checked', false);
			}

			function check_instrument() {
				if($('#instrument_none').is(':checked'))
					$('#instrument_none').prop('checked', false);
				if($('#instrument_all').is(':checked'))
					$('#instrument_all').prop('checked', false);
			}

		</script>

	</head>

	<body background="page_bg.jpg">

		<h1>Generate Reports</h1>

		<br><br>
		<button id="delhi">Delhi/NCR</button>
		<button id="rest">Rest</button>
		<form method="POST" action="reports_generate.php" onSubmit="return submit_form();">

		<div style="width:50%;float:left">

			State: <br><br>
				<input type="checkbox" id="state_none" name="state" value="none">None</input>
				<input type="checkbox" id="state_all" name="state" value="all" checked="checked">All States</input>

				<?php

					for($i = 0; $i < count($states); $i ++) {
						if($i % 5 == 0)
							echo '<br/>';
						?>

						<input type="checkbox" class="state" onclick="check_state()" name="state[<?php echo $i + 1; ?>]" value="<?php echo $states[$i]; ?>"><?php echo $states[$i]; ?>

						<?php

					}

				?>

		</div>
		<div style="width:50%; float:left;">
		Instrument: <br><br>
			<input type="checkbox" id="instrument_none" name="instrument" value="none">None</input>
			<input type="checkbox" id="instrument_all" name="instrument" value="all" checked="checked">All Instruments</input>
			<?php

				for($i = 0; $i < count($instruments); $i ++) {
					if($i % 5 == 0)
						echo '<br>';
					?>

					<input type="checkbox" class="instrument" onclick="check_instrument()" name="instrument[<?php echo $i+ 1; ?>]" value="<?php echo $instruments[$i]; ?>"><?php echo $instruments[$i]; ?>

					<?php

				}

			?>
		</div>
		<br><br>

		<input type="submit" id="submit" value="Go" style="position:absolute;bottom:30%;left:45%;">

		</form>

	</body>

</html>
