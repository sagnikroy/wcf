<?php

  session_start();

  if(!isset($_SESSION['user']))
		header('location:login.php');
	$role = intval($_SESSION['role']);

?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Institutes</title>
</head>

<body background="page_bg.jpg">

<h1>Institutes</h1>

<br/><br/>

<table cellspacing="0" width="100%" border="3">

  <tr>
    <th>S.No.</th>
    <th>Institute</th>
    <th>City</th>
    <th>State</th>
  </tr>

<?php

  include 'connection.php';

  $query = "SELECT DISTINCT(CONCAT(institute, ':', state, ':', city)) AS concat
    FROM users
    WHERE city <> '' AND institute <> '' AND state <> '' AND institute <> 'NA'
    GROUP BY state, city, institute ";
  $result = mysql_query($query, $con);

  $num_rows = mysql_num_rows($result);

  for($i = 0; $i < $num_rows; $i ++) {

    $concat = mysql_result($result, $i, "concat");
    $concat_array = explode(":", $concat);
    $institute = ucwords(strtolower($concat_array[0]));
    $state = ucwords(strtolower($concat_array[1]));
    $city = ucwords(strtolower($concat_array[2]));

    ?>

  <tr align="center">
    <td><?php echo $i + 1; ?></td>
    <td>
      <a href="institute_detail.php?param=<?php echo str_replace("&", "%26", $concat); ?>">
        <?php echo $institute; ?>
      </a>
    </td>
    <td><?php echo $city; ?></td>
    <td><?php echo $state; ?></td>
  </tr>

    <?php

  }

  mysql_close($con);

?>

</table>

</body>
</html>
