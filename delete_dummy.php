<?php	
	
	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	if($_SESSION['role'] != '1')
		header('location:index.php');
		
	include "connection.php";
	
	$states = "";
	$instruments = "";
	$ids = "";
	
	$query = "SELECT State FROM state";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$states = $states.', \''.mysql_result($result, $i, "State").'\'';
	}
	
	$query = "SELECT Instrument FROM instrument";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$instruments = $instruments.', \''.mysql_result($result, $i, "Instrument").'\'';
	}
	
	$states = '('.substr($states, 2).')';
	$instruments = '('.substr($instruments, 2).')';
	
	$ids = "";
	
	$query = "SELECT id FROM users WHERE state NOT IN $states OR instrument NOT IN $instruments";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$ids = $ids.', '.mysql_result($result, $i, "id");
	}
	
	$ids = '('.substr($ids, 2).')';
	
	$query = "DELETE FROM users WHERE id IN $ids";
	$result = mysql_query($query, $con);
	
	header('location: dummy.php');
	
?>