<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	if(!isset($_POST['state']) || !isset($_POST['instrument']))
		die("Please select state/instrument");
	
	$case = 0;
	$state = $_POST['state'];
	$instrument = $_POST['instrument'];
	$header;
	
	$total_count = 0;
	
	if($state == 'all' && $instrument == 'all') {
		
		$case = 1;
		
		$all_states = array();
		$all_instruments = array();
		
		$query = "SELECT DISTINCT state FROM users;";
		$result = mysql_query($query, $con);
		
		for($i = 0; $i < mysql_num_rows($result); $i++) {
			$all_states[] = mysql_result($result, $i, "state");
		}
		
		$query = "SELECT DISTINCT instrument FROM users;";
		$result = mysql_query($query, $con);
		
		for($i = 0; $i < mysql_num_rows($result); $i++) {
			$all_instruments[] = mysql_result($result, $i, "instrument");
		}
		
		
		
		
	} else {
		
		if($state == 'none' && $instrument == 'none') {
		
			echo "Can't select none from both!";
			
		} else if($state == 'all' && $instrument == 'none') {
			
			$query = "SELECT state, COUNT(*) count FROM users GROUP BY state";
			$header = "State";
			
		} else if($instrument == 'all' && $state == 'none') {
			
			$query = "SELECT instrument, COUNT(*) count FROM users GROUP BY instrument";
			$header = "Instrument";
			
		} else if($state == 'all' || $state == 'none') {
			
			$instruments = '\''.strtolower(implode('\',\'', $_POST['instrument'])).'\'';
			$query = "SELECT state, COUNT(*) count FROM users WHERE instrument IN ($instruments) GROUP BY state";
			$header = 'State';
			
		} else if ($instrument == 'all') {
			
			$states = '\''.strtolower(implode('\',\'', $_POST['state'])).'\'';
			$query = "SELECT instrument, COUNT(*) count FROM users WHERE state IN ($states) GROUP BY instrument";
			$header = 'Instrument';
			
		} else if($instrument == 'none') {
			
			$states = '\''.strtolower(implode('\',\'', $_POST['state'])).'\'';
			$query = "SELECT city, COUNT(*) count FROM users WHERE state IN ($states) GROUP BY city";
			$header = 'City';
			
		} else {
			
			$states = '\''.strtolower(implode('\',\'', $_POST['state'])).'\'';
			$instruments = '\''.strtolower(implode('\',\'', $_POST['instrument'])).'\'';
			$query = "SELECT city, COUNT(*) count FROM users WHERE state IN ($states) AND instrument IN ($instruments) GROUP BY city";
			$header = 'City';
			
		}
		
	}
	
	$result = mysql_query($query, $con) or die(mysql_error());

?>

<html>

	<head>
	
		<title>Custom Report</title>
		
	</head>
	
	<body background="page_bg.jpg">
	
		<h1>Result: </h1>
		
		<br><br>
		
		<?php 
			if($case == 0) {
				
		?>
		<form method="POST" action="download_csv.php">
			
			<input type="hidden" name="query" value="<?php echo $query; ?>">
			<input type="hidden" name="loc" value="custom">
			<input type="hidden" name="header" value="<?php echo $header; ?>">
			<input type="submit" value="Download CSV">
		
		</form>
		
		<br><br>
		
		<?php
			}
		?>
		
		<table cellspacing="0" border="3" width="100%" cellpadding="5">
		
		<?php
		
			if($case == 0) {
		
		?>
		
		<tr align="center">
			<th>S.No.</th>
			<th><?php echo $header; ?></th>
			<th>Count</th>
		</tr>
		
		<?php
		
			for($i = 0; $i < mysql_num_rows($result); $i ++) {
				
				$total_count += intval(mysql_result($result, $i, "count"));
				
				?>
				
				<tr align="center">
					<td><?php echo $i + 1; ?></td>
					<td><?php echo mysql_result($result, $i, strtolower($header)); ?></td>
					<td><?php echo mysql_result($result, $i, "count"); ?></td>
				</tr>
				
				<?php
				
			}
			
		?>
		
		</table>
		
		<br><br>
		
		Total no. of result rows: <?php echo $total_count; ?>
		
		<?php
		
			} else {
			
		?>
		
		<tr>
			<th>Instrument/State</th>
			<?php
				for($i = 0; $i < count($all_states); $i ++) {
					?>
					<td><?php echo $all_states[$i]; ?></td>
					<?php
				}
			?>
			<th>Total</th>
		</tr>
		
			<?php
				
				for($i = 0; $i < count($all_instruments); $i ++) {
					$total = 0;
					?>
					<tr>
					<td><?php echo $all_instruments[$i]; ?></td>
					<?php
					for($j = 0; $j < count($all_states); $j ++) {
						
						$state = $all_states[$j];
						$instrument = $all_instruments[$i];
						$query = "SELECT name FROM users WHERE state='$state' AND instrument='$instrument'";
						$result = mysql_query($query, $con);
						$total += mysql_num_rows($result);
						?>
						<td><?php echo mysql_num_rows($result); ?></td>
						<?php
					}
					
					?>
					<td><?php echo $total; ?></td>
					</tr>
					<?php
				}
			?>
		<tr>
			<th>Total</th>
			<?php
				$temp = $i + 2;
				for($i = 0; $i < count($all_states); $i ++) {
					$total;
					$query = "SELECT name FROM users WHERE state='$all_states[$i]'";
					$result = mysql_query($query, $con);
					$total += mysql_num_rows($result);
					?>
					<td><?php echo mysql_num_rows($result); ?></td>
					<?php
				}
			
				
			?>
			<td><?php echo $total - 1; ?></td>
		</tr>
		
		</table>
		
		<br><br>
		<form action="consolidated_report.php" method="POST">
			<center><input type="submit" value="Download CSV"></center>
		</form>
		
		<?php
		
			}
			
			mysql_close($con);
		?>
	
	</body>
	
</html>