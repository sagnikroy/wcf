<?php

  session_start();

  if(!isset($_SESSION['user']))
    header('location:login.php');

  include 'connection.php';

  $dep_states = array();
	$dep_stns = array();
  $dep_dates = array();


  $query = "SELECT state FROM users WHERE state <> '' GROUP BY state;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$dep_states[$i] = mysql_result($result, $i, "state");

	}

  $query = "SELECT dep_stn FROM users WHERE dep_stn <> '' GROUP BY dep_stn;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$dep_stns[$i] = mysql_result($result, $i, "dep_stn");

	}

  $query = "SELECT dep_date FROM users WHERE dep_date <> '1970-1-1' GROUP BY dep_date;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$dep_dates[$i] = mysql_result($result, $i, "dep_date");

	}

	mysql_close($con);

?>

<html>
<head>
  <title>View Train Data</title>
</head>
<body background="page_bg.jpg">

  <h1>View Train Info</h1>
  <br><br>

  <form method="GET" action="train_agg.php">
    <table width="80%" border="0" cellspacing="10" cellpadding="5">
        <input type="hidden" name="source" value="dep"/>
        <tr align="right">
          <td>
            State: <select name="state">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($dep_states); $i ++) {
                  ?>
                  <option value="<?php echo $dep_states[$i]; ?>"><?php echo ucwords($dep_states[$i]); ?></option>
                  <?php
                }
               ?>
            </select>
          </td>
          <td>
            Departure Station: <select name="dep_stn">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($dep_stns); $i ++) {
                  ?>
                  <option value="<?php echo $dep_stns[$i]; ?>"><?php echo $dep_stns[$i]; ?></option>
                  <?php
                }
               ?>
            </select>
          </td>
          <td>
            Departure Date: <select name="dep_date">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($dep_dates); $i ++) {
                  ?>
                  <option value="<?php echo $dep_dates[$i]; ?>"><?php echo $dep_dates[$i]; ?></option>
                  <?php
                }
               ?>
            </select>
          </td>
      </tr>
      <tr align="center">
        <td colspan="4">
          <input type="submit" value="Go">
        </td>
      </tr>
    </table>

  </form>

</body>
</html>
