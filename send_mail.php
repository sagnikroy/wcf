<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	if(empty($_POST['query']))
		die('Access Denied');
	
	$query = str_replace("*", "email", $_POST['query']);
	$result = mysql_query($query, $con);
	
	$emails = array();
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		$email = mysql_result($result, $i, "email");
		$email = trim($email);
		$email = str_replace(' ', '', $email);
		if($email != "")
			$emails[] = strtolower($email);
	}
	
	if(count($email) == 0)
		echo "There are no valid emails.";
	else {
		
?>

<html>

	<head>
	
		<title>Send Emails</title>
		
	</head>
	
	<body background="page_bg.jpg">
		<center>
		
			<form method="POST" action="mailer.php">
			
				<h2>Send Emails</h2>
				<p><?php echo count($emails); ?> valid emails found</p>
				<br><br>
				
				
				<table align="center" cellspacing="9" cellpadding="9" border="0">
				
					<tr>
						<td align="right">To: </td>
						<td align="left">
							<textarea rows="10" cols="70" name="emails"><?php echo implode(",", $emails); ?></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">Subject: </td>
						<td align="left"><input type="text" name="subject" size="68"></td>
					</tr>
					<tr>
						<td align="right">Body: </td>
						<td align="left"><textarea name="body" rows="10" cols="70"></textarea></td>
					</tr>
					<tr>
						<td align="right">Reply To: </td>
						<td align="left"><input type="text" name="reply" value="participation@worldculturefestival.in"></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="submit" value="Send Emails">
						</td>
					</tr>
				
				</table>
				
			</form>
		
		</center>
	
	</body>

</html>

<?php
		
	}
	
	mysql_close($con);

?>