-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2015 at 10:01 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wcf`
--

-- --------------------------------------------------------

--
-- Table structure for table `cred`
--

CREATE TABLE IF NOT EXISTS `cred` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `pass` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cred`
--

INSERT INTO `cred` (`id`, `user`, `pass`) VALUES
(1, 'admin', '62d51edba99b0e59884bb083cf222693');

-- --------------------------------------------------------

--
-- Table structure for table `idproof`
--

CREATE TABLE IF NOT EXISTS `idproof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ID_proof` varchar(100) DEFAULT NULL,
  UNIQUE KEY `Sr No` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `idproof`
--

INSERT INTO `idproof` (`id`, `ID_proof`) VALUES
(1, 'Aadhar Card'),
(2, 'Passport'),
(3, 'Driving Licence'),
(4, 'Voter ID'),
(5, 'PAN Card'),
(6, 'Student ID with Photograph'),
(7, 'ID card issued by Govt. bodies');

-- --------------------------------------------------------

--
-- Table structure for table `instrument`
--

CREATE TABLE IF NOT EXISTS `instrument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Instrument` varchar(100) DEFAULT NULL,
  `Code` varchar(2) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `instrument`
--

INSERT INTO `instrument` (`id`, `Instrument`, `Code`) VALUES
(1, 'BANJO', 'BJ'),
(2, 'BIHU DHOL', 'BD'),
(3, 'CHANDA', 'CH'),
(4, 'CLARINET', 'CL'),
(5, 'DHAFF', 'DH'),
(6, 'DHOL', 'DL'),
(7, 'DHOL DHAMAU', 'DD'),
(8, 'DHOLAK', 'DK'),
(9, 'DOLLU', 'DU'),
(10, 'DUFFLI', 'DF'),
(11, 'EDAKKAM', 'EK'),
(12, 'FLUTE', 'FL'),
(13, 'GHATAM', 'GH'),
(14, 'GUITAR', 'GU'),
(15, 'HARMONIUM', 'HM'),
(16, 'HUDKA', 'HU'),
(17, 'KANJEERA', 'KJ'),
(18, 'KHOL', 'KH'),
(19, 'MANDOLIN', 'MD'),
(20, 'MOUTH ORGAN', 'MO'),
(21, 'MRIDANGAM', 'MR'),
(22, 'NAAL', 'NL'),
(23, 'NAASIK DHOL', 'ND'),
(24, 'NAGARA', 'NG'),
(25, 'PAKHAWAJ', 'PK'),
(26, 'RABAB', 'RB'),
(27, 'SAARANGI', 'SR'),
(28, 'SANTOOR', 'SN'),
(29, 'SAROD', 'SD'),
(30, 'SAXOPHONE', 'SX'),
(31, 'SHEHNAI', 'SH'),
(32, 'SITAR', 'ST'),
(33, 'TAASHA', 'TA'),
(34, 'TABLA', 'TB'),
(35, 'TAVIL', 'TV'),
(36, 'TIMBALI', 'TI'),
(37, 'TRUMPET', 'TR'),
(38, 'TUMBAKNAER', 'TU'),
(39, 'VEENA', 'VN'),
(40, 'VIOLIN', 'VI'),
(41, 'CONGO', 'CO'),
(42, 'DJEMBE', 'DJ'),
(43, 'DILRUBA', 'DB'),
(44, 'TAUS', 'TS');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `type` varchar(10) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `date`, `time`, `type`, `start`, `end`) VALUES
(6, '2015-10-12', '01:35:55', 'HC', 530, 530),
(7, '2015-10-12', '01:42:17', 'HC', 530, 1291),
(8, '2015-10-12', '01:48:00', 'HC', 7002, 7763);

-- --------------------------------------------------------

--
-- Table structure for table `method`
--

CREATE TABLE IF NOT EXISTS `method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Method` varchar(14) DEFAULT NULL,
  `Code` varchar(2) DEFAULT NULL,
  UNIQUE KEY `Sr No` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `method`
--

INSERT INTO `method` (`id`, `Method`, `Code`) VALUES
(1, 'Hard Copies', 'HC'),
(2, 'Scanned Copies', 'SC'),
(3, 'State Excel', 'SE'),
(4, 'Google Form', 'GF'),
(5, 'Ashram Website', 'AW');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE IF NOT EXISTS `qualification` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `Qualification` varchar(7) DEFAULT NULL,
  `Experience` varchar(9) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`id`, `Qualification`, `Experience`) VALUES
(1, 'Diploma', '< 1 yr'),
(2, 'Degree', '1 - 2 yrs'),
(3, 'Others', '3 - 5 yrs'),
(4, 'None', '> 5 yrs');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `State` varchar(17) DEFAULT NULL,
  `Code` varchar(3) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `State`, `Code`) VALUES
(1, 'Andhra Pradesh', 'ADP'),
(2, 'Arunachal Pradesh', 'ANP'),
(3, 'Assam', 'ASM'),
(4, 'Bihar', 'BHR'),
(5, 'Chandigarh', 'CHD'),
(6, 'Chhattisgarh', 'CHG'),
(7, 'Delhi (Central)', 'DLC'),
(8, 'Delhi (East)', 'DLE'),
(9, 'Delhi (North)', 'DLN'),
(10, 'Delhi (South)', 'DLS'),
(11, 'Delhi (West)', 'DLW'),
(12, 'Goa', 'GOA'),
(13, 'Gujarat', 'GUJ'),
(14, 'Haryana', 'HRY'),
(15, 'Himachal Pradesh', 'HMP'),
(16, 'Jammu and Kashmir', 'JNK'),
(17, 'Jharkhand', 'JHK'),
(18, 'Karnataka', 'KRN'),
(19, 'Kerela', 'KRL'),
(20, 'Madhya Pradesh', 'MDP'),
(21, 'Maharashtra', 'MHR'),
(22, 'Manipur', 'MNP'),
(23, 'Meghalaya', 'MGH'),
(24, 'Mizoram', 'MIZ'),
(25, 'Nagaland', 'NAG'),
(26, 'NCR (Dwarka)', 'NDW'),
(27, 'NCR (Faridabad)', 'NFR'),
(28, 'NCR (Ghaziabad)', 'NGZ'),
(29, 'NCR (Gurgaon)', 'NGU'),
(30, 'NCR (Noida)', 'NND'),
(31, 'Odisha', 'ODI'),
(32, 'Outside India', 'NRI'),
(33, 'Punjab', 'PUN'),
(34, 'Rajasthan', 'RJN'),
(35, 'Sikkim', 'SKM'),
(36, 'Tamil Nadu', 'TMN'),
(37, 'Telangana', 'TLN'),
(38, 'Tripura', 'TRI'),
(39, 'Uttar Pradesh', 'UTP'),
(40, 'Uttarakhand', 'UTK'),
(41, 'West Bengal', 'WBN'),
(42, 'Puducherry', 'PUD');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` int(11) NOT NULL,
  `fh_name` varchar(100) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `id_ref` varchar(100) NOT NULL,
  `id_no` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `instrument` varchar(100) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `experience` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
