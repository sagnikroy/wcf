<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	if($_SESSION['role'] != '1')
		header('location:index.php');
	
	include 'connection.php';
	
	$query = "SELECT * FROM logs_delete ORDER BY date DESC, time DESC";
	$result = mysql_query($query, $con);
	
?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Delete Logs</title>
</head>
<body background="page_bg.jpg">

<h1>Delete Logs</h1>
<table border="3" width="70%" cellspacing="0">

<tr>
	<th>SNo</th>
	<th>Date</th>
	<th>Time</th>
	<th>States</th>
	<th>Instruments</th>
	<th>No. of Deleted Records</th>
</tr>

<?php

	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		
		$id = mysql_result($result, $i, "id");
		$date = mysql_result($result, $i, "date");
		$time = mysql_result($result, $i, "time");
		$states = mysql_result($result, $i, "states");
		$instruments = mysql_result($result, $i, "instruments");
		$number = mysql_result($result, $i, "number");
		
		?>
		
		<tr align="center">
		
			<td><?php echo $i + 1; ?></td>
			<td><?php echo $date; ?></td>
			<td><?php echo $time; ?></td>
			<td><?php echo $states; ?></td>
			<td><?php echo $instruments; ?></td>
			<td><?php echo $number; ?></td>
		
		</tr>
		
		<?php
		
	}
	
?>

</table>

<?php
	
	mysql_close($con);

?>