<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	$instruments = array();
	$states = array();
	
	$query = "SELECT Instrument FROM instrument;";;
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		
		$instruments[$i] = mysql_result($result, $i, "Instrument");
		
	}
	
	$query = "SELECT State FROM state;";;
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		
		$states[$i] = mysql_result($result, $i, "State");
		
	}
	
	mysql_close($con);

?>

<html>

	<head>
	<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
	
		<title>Delete Data</title>
		<script src="jquery.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
			
				$('#state_none').click(function() {
					$('#state_none').prop('checked', true);
					$('.state').prop('checked', false);
				});
				
				$('#instrument_none').click(function() {
					$('#instrument_none').prop('checked', true);
					$('.instrument').prop('checked', false);
				});
				
				$('#submit').click(function() {
					if($('#instrument_none').is(':checked') &&
						$('#state_none').is(':checked')) {
							alert('Can\'t select none for both!');
							return false;
						}
				});
			
			});
			
			function submit_form() {
				
				var state = document.getElementById('state').value;
				var instrument = document.getElementById('instrument').value;
				
				if(state == instrument) {
					alert('Can\'t select none for both!');
					return false;
				}
				
				return true;
			}
			
			function check_state() {
				if($('#state_none').is(':checked'))
					$('#state_none').prop('checked', false);
			}
			
			function check_instrument() {
				if($('#instrument_none').is(':checked'))
					$('#instrument_none').prop('checked', false);
			}
		
		</script>
		
	</head>
	
	<body background="page_bg.jpg">
	
		<h1>Delete Data</h1>
		
		<br><br>
		
		<form method="POST" action="action_delete.php" onSubmit="return submit_form();">
		
		<div style="width:50%;float:left">
		
			State: <br><br>
				<input type="checkbox" id="state_none" name="state" checked="checked" value="none">None</input>
				
				<?php
				
					for($i = 0; $i < count($states); $i ++) {
						if($i % 5 == 0)
							echo '<br/>';
						?>
						
						<input type="checkbox" class="state" onclick="check_state()" name="state[<?php echo $i + 1; ?>]" value="<?php echo $states[$i]; ?>"><?php echo $states[$i]; ?>
						
						<?php
						
					}
				
				?>
				
		</div>
		<div style="width:50%; float:left;">
		Instrument: <br><br>
			<input type="checkbox" id="instrument_none" name="instrument" checked="checked" value="none">None</input>

			<?php
			
				for($i = 0; $i < count($instruments); $i ++) {
					if($i % 5 == 0)
						echo '<br>';
					?>
					
					<input type="checkbox" class="instrument" onclick="check_instrument()" name="instrument[<?php echo $i+ 1; ?>]" value="<?php echo $instruments[$i]; ?>"><?php echo $instruments[$i]; ?>
					
					<?php
					
				}
			
			?>
		</div>
		<br><br>
		
		<input type="submit" id="submit" value="Go" style="position:absolute;bottom:30%;left:45%;">
		
		</form>
	
	</body>
	
</html>