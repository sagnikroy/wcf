<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	$consolidated_array = array();
		
	$all_states = array();
	$all_instruments = array();
	
	$query = "SELECT DISTINCT state FROM users;";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i++) {
		$all_states[] = mysql_result($result, $i, "state");
	}
	
	$query = "SELECT DISTINCT instrument FROM users;";
	$result = mysql_query($query, $con);
	
	for($i = 0; $i < mysql_num_rows($result); $i++) {
		$all_instruments[] = mysql_result($result, $i, "instrument");
	}
	
	$consolidated_array[0][0] = "Instrument/State";
	for($i = 0; $i < count($all_states); $i ++) {
		$consolidated_array[0][$i+1] = $all_states[$i];
	}
	
	for($i = 0; $i < count($all_instruments); $i ++) {
		$total = 0;
		$consolidated_array[$i+1][0] = $all_instruments[$i];
		for($j = 0; $j < count($all_states); $j ++) {
			
			$state = $all_states[$j];
			$instrument = $all_instruments[$i];
			$query = "SELECT name FROM users WHERE state='$state' AND instrument='$instrument'";
			$result = mysql_query($query, $con);
			$total += mysql_num_rows($result);
			$consolidated_array[$i+1][$j+1] = mysql_num_rows($result);
		}
		$consolidated_array[$i+1][] = $total;
	}
	
	$temp = $i + 2;
	$consolidated_array[$temp][0] = "Total";
	for($i = 0; $i < count($all_states); $i ++) {
		$total;
		$query = "SELECT name FROM users WHERE state='$all_states[$i]'";
		$result = mysql_query($query, $con);
		$total += mysql_num_rows($result);
		$consolidated_array[$temp][$i+1] = mysql_num_rows($result);
	}
	
	$consolidated_array[$temp][] = $total;
	
	mysql_close($con);
	
	header("Content-Type: text/csv");
	header('Content-disposition: attachment;filename=consolidated.csv');
	
	$fp = fopen("php://output", "w");
	
	foreach($consolidated_array as $row){
        fputcsv($fp, $row, ",");
    }
    fclose($fp);

?>