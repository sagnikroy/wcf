<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	if($_SESSION['role'] != '1')
		header('location:index.php');

	include 'connection.php';

	$query = "SELECT Method, Code FROM method;";
	$result = mysql_query($query, $con);

	mysql_close($con);

?>

<html>

	<head>

		<title>Upload Applications</title>

	</head>

	<body background="page_bg.jpg">

		<h1>Upload your CSVs Here</h1>

		<form action="upload_csv.php" method="POST" enctype="multipart/form-data">

			Type: <input type="radio" name="type" value="insert" checked="checked">Insert
			<input type="radio" name="type" value="update">Update
			<input type="radio" name="type" value="delete">Delete<br/><br/>

			Start reading data from row no.: <input type="text" name="offset" /><br/><br/>

			Form of Entry:

			<select name="entry">
				<?php

					for($i = 0; $i < mysql_num_rows($result); $i ++) {

						?>

						<option value="<?php echo mysql_result($result, $i, "Code"); ?>">
							<?php echo mysql_result($result, $i, "Method"); ?>
						</option>

						<?php

					}

				?>
			</select>

			<br/><br/>

			Upload file: <input type="file" name="csvFile"><br/><br/>

			<input type="submit" value="Upload" />

		</form>

	</body>

</html>
