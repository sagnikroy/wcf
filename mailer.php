<?php

	session_start();
	
	if(empty($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	if(empty($_POST['emails']) || empty($_POST['subject']) || empty($_POST['body']) || empty($_POST['reply']))
		echo 'Please fill all fields';
	else {
		
		$emails = explode(",", $_POST['emails']);
		$from = "participation@worldculturefestival.in";
		$subject = $_POST['subject'];
		$reply = $_POST['reply'];
		$body = $_POST['body'];
		
		$faulty_emails = array();
		
		for($i = 0; $i < count($emails); $i ++) {
			
			$to = trim($emails[$i]);
			$body = wordwrap($body, 70, "\r\n");
			$headers = 'From: '.$from."\r\n".'Reply-To: '.$reply."\r\n";
			$headers .= "MIME-Version: 1.0"."\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8\r\n";
			if(filter_var($to, FILTER_VALIDATE_EMAIL))
				mail($to, $subject, $body, $headers);
			else {
				$faulty_emails[] = $to;
			}
			
		}
		
		echo ($i - count($faulty_emails))." emails sent successfully.";
		if(count($faulty_emails) > 0)
			echo "\n\nCould not send to".implode(",", $faulty_emails);
		
	}	
	
	mysql_close($con);
	
?>
