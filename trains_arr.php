<?php

  session_start();

  if(!isset($_SESSION['user']))
    header('location:login.php');

  include 'connection.php';

  $arr_states = array();
	$arr_stns = array();
  $arr_dates = array();

	$query = "SELECT state FROM users WHERE state <> '' GROUP BY state;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$arr_states[$i] = mysql_result($result, $i, "state");

	}

  $query = "SELECT arr_stn FROM users WHERE arr_stn <> '' GROUP BY arr_stn;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$arr_stns[$i] = mysql_result($result, $i, "arr_stn");

	}

  $query = "SELECT arr_date FROM users WHERE arr_date <> '1970-1-1' GROUP BY arr_date;";
	$result = mysql_query($query, $con);

	for($i = 0; $i < mysql_num_rows($result); $i ++) {

		$arr_dates[$i] = mysql_result($result, $i, "arr_date");

	}

	mysql_close($con);

?>

<html>
<head>
  <title>View Train Data</title>
</head>
<body background="page_bg.jpg">

  <h1>View Train Info</h1>
  <br><br>

  <form method="GET" action="train_agg.php">
    <input type="hidden" name="source" value="arr"/>
    <table width="80%" border="0" cellspacing="10" cellpadding="5">
        <tr align="right">
          <td>

            State: <select name="state">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($arr_states); $i ++) {
                  ?>
                  <option value="<?php echo $arr_states[$i]; ?>"><?php echo ucwords($arr_states[$i]); ?></option>
                  <?php
                }
               ?>
            </select>

          </td>
          <td>

            Arrival Station: <select name="arr_stn">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($arr_stns); $i ++) {
                  ?>
                  <option value="<?php echo $arr_stns[$i]; ?>"><?php echo $arr_stns[$i]; ?></option>
                  <?php
                }
               ?>
            </select>

          </td>

          <td>

            Arrival Date: <select name="arr_date">
              <option value="all">All</option>
              <?php
                for($i = 0; $i < count($arr_dates); $i ++) {
                  ?>
                  <option value="<?php echo $arr_dates[$i]; ?>"><?php echo $arr_dates[$i]; ?></option>
                  <?php
                }
               ?>
            </select>

          </td>

        </tr>
      <tr align="center">
        <td colspan="4">
          <input type="submit" value="Go">
        </td>
      </tr>
    </table>

  </form>

</body>
</html>
