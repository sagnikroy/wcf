<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');
	$role = intval($_SESSION['role']);

?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Home Page</title>
</head>
<body align="center" background="BG_White.jpg">
	<img src="artofliving_logo.png">
	<h1>Welcome to WCF Back End</h1>
	<p>Options</p>
	<?php if($role == 1) { ?>
	1. <a href="upload.php" target="_blank">Upload New CSV</a><br/>
	<?php } ?>
	<?php echo ($role == 1 ? '2':'1'); ?>. <a href="data.php" target="_blank">View Data / Send Email</a><br/>
	<?php echo $role == 1 ? '3':'2'; ?>. <a href="reports.php" target="_blank">View Reports</a><br/>
	<?php echo $role == 1 ? '4':'3'; ?>. <a href="search.php" target="_blank">Search Data</a><br/>
	<?php echo $role == 1 ? '5':'4'; ?>. <a href="institute.php" target="_blank">View Institutes</a><br/>
	<?php if($role == 1) { ?>
	6. <a href="trains_arr.php" target="_blank">Arrival Train Info</a><br/>
	7. <a href="trains_dep.php" target="_blank">Departure Train Info</a><br/>
	8. <a href="train_agg.php" target="_blank">Aggregated Train Info</a><br/>
	9. <a href="dummy.php" target="_blank">Dummy Data</a><br/>
	10. <a href="logs.php" target="_blank">View Logs</a><br/>
	11. <a href="mass_delete.php" target="_blank">Delete Data</a><br/>
	12. <a href="delete_logs_list.php" target="_blank">Delete Logs</a><br/>
	<?php } ?>
	<br/><br/><a href="logout.php">Logout</a>
	<br>
	<br>
	<br>
	<img src="Flyer_Top.jpg" width = "40%">
	</ul>
</body>
</html>
