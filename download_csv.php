<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	if(!isset($_POST['query']))
		die("Please follow the proper link.");

	include 'connection.php';

	$query = $_POST['query'];
	$loc = $_POST['loc'];
	$result = mysql_query($query, $con);

	$array_headers = array();

	if($loc == 'users') {
		$array_headers[] = "S.No";
		$array_headers[] = "Generated ID";
		$array_headers[] = "Name";
		$array_headers[] = "Gender";
		$array_headers[] = "Age";
		$array_headers[] = "Father's/Husband's Name";
		$array_headers[] = "Institute";
		$array_headers[] = "ID Type";
		$array_headers[] = "ID Number";
		$array_headers[] = "Mobile";
		$array_headers[] = "Email";
		$array_headers[] = "Address";
		$array_headers[] = "City";
		$array_headers[] = "Pincode";
		$array_headers[] = "State";
		$array_headers[] = "Instrument";
		$array_headers[] = "Qualification";
		$array_headers[] = "Experience";
		$array_headers[] = "Remarks";
		$array_headers[] = "Volunteer Name";
		$array_headers[] = "Volunteer Mobile";
		$array_headers[] = "Volunteer Email";
		$array_headers[] = "Method of Transport";
		$array_headers[] = "Arrival Train PNR";
		$array_headers[] = "Arrival Train";
		$array_headers[] = "Arrival Station";
		$array_headers[] = "Arrival Date";
		$array_headers[] = "Arrival Time";
		$array_headers[] = "Departure Train PNR";
		$array_headers[] = "Departure Train";
		$array_headers[] = "Departure Station";
		$array_headers[] = "Departure Date";
		$array_headers[] = "Departure Time";
	} else {
		$array_headers[] = $_POST['header'];
		$array_headers[] = "Count";
	}

	$array_blanks = array();

	header( 'Content-Type: text/csv' );
	header( 'Content-Disposition: attachment;filename=download.csv');
	$fp = fopen('php://output', 'w');

	fputcsv($fp, $array_headers);
	fputcsv($fp, $array_blanks);

	while($row = mysql_fetch_assoc($result)) {
		fputcsv($fp, $row);
	}

	fclose($fp);

	mysql_close($con);

?>
