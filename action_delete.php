<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	include 'connection.php';
	
	if(!isset($_POST['state']) || !isset($_POST['instrument']))
		die("Please select state/instrument");

	$state = $_POST['state'];
	$instrument = $_POST['instrument'];
	$states = "";
	$instruments = "";

	if($state == 'none') {

		$instrument_list = '\''.strtolower(implode('\',\'', $instrument)).'\'';
		$instrument = implode(',', $instrument);
		$query = "DELETE FROM users WHERE instrument IN ($instrument_list);";

	} else if($instrument == 'none') {

		$state_list = '\''.strtolower(implode('\',\'', $state)).'\'';
		$states = implode(',', $state);
		$query = "DELETE FROM users WHERE state IN ($state_list);";

	} else {

		$state_list = '\''.strtolower(implode('\',\'', $state)).'\'';
		$instrument_list = '\''.strtolower(implode('\',\'', $instrument)).'\'';

		$instruments = implode(',', $instrument);
		$states = implode(',', $state);

		$query = "DELETE FROM users WHERE state IN ($state_list) AND instrument IN ($instrument_list);";

	}

	$date = date('Y-m-d', time());
	$time = date('H:i:s', time());

	$result = mysql_query($query, $con);
	
	$num_deleted = mysql_affected_rows();

	$query = "INSERT INTO logs_delete (date, time, states, instruments, number)
				VALUES ('$date', '$time', '$states', '$instruments', '$num_deleted');";
	$result = mysql_query($query, $con);

	echo "Successfully Deleted ".$num_deleted." rows";

?>