<?php

	session_start();
	
	if(!isset($_SESSION['user']))
		header('location:login.php');
	
	if($_SESSION['role'] != '1')
		header('location:index.php');
	
	include 'connection.php';
	
	$query = "SELECT * FROM logs ORDER BY date DESC, time DESC";
	$result = mysql_query($query, $con);
	
?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Logs</title>
<script type="text/javascript">
	function del(id, start, end) {
		if(window.confirm("Are you sure?")) {
			window.location.replace("delete_log.php?id=" + id + "&start=" + start + "&end=" + end);
		}
		return false;
	}
</script>
</head>
<body background="page_bg.jpg">

<h1>Logs</h1>
<table border="3" width="70%" cellspacing="0">

<tr>
	<th>SNo</th>
	<th>Type</th>
	<th>Date</th>
	<th>Time</th>
	<th>No. of Inserted Records</th>
	<th>View</th>
	<th>Delete</th>
</tr>

<?php

	for($i = 0; $i < mysql_num_rows($result); $i ++) {
		
		$id = mysql_result($result, $i, "id");
		$type = mysql_result($result, $i, "type");
		$date = mysql_result($result, $i, "date");
		$time = mysql_result($result, $i, "time");
		$start = mysql_result($result, $i, "start");
		$end = mysql_result($result, $i, "end");
		?>
		
		<tr align="center">
		
			<td><?php echo $i + 1; ?></td>
			<td><?php echo $type; ?></td>
			<td><?php echo $date; ?></td>
			<td><?php echo $time; ?></td>
			<td><?php echo (intval($end) - intval($start)); ?></td>
			<td><a href="view_log.php?start=<?php echo $start; ?>&end=<?php echo $end; ?>" target="_blank">View</a></td>
			<td>
				<a href="#" onclick="del('<?php echo $id; ?>','<?php echo $start; ?>','<?php echo $end; ?>')">
					Delete
				</a>
			</td>
		
		</tr>
		
		<?php
		
	}
	
?>

</table>

<?php
	
	mysql_close($con);

?>