<?php

  session_start();

  if(!isset($_SESSION['user']))
    header('location:login.php');

  include 'connection.php';

  $query ="SELECT DISTINCT arr_date FROM users WHERE arr_date <> '1970-01-01' AND mode_transport = 'train'
            ORDER BY arr_date;";
  $result = mysql_query($query, $con) or die(mysql_error());

  $dates = array();

?>

<html>
  <head>
    <title>Aggregated Train Info</title>
  </head>
  <body>
    <h1>Arrival Schedule</h1>

<?php

  for($i = 0; $i < mysql_num_rows($result); $i ++) {
    $dates[] = mysql_result($result, $i, "arr_date");
  }

  for($i = 0; $i < count($dates); $i ++) {
    $date = $dates[$i];
    $query = "SELECT arr_time, arr_trn, state, arr_stn, COUNT(*) AS cnt
              FROM users
              WHERE arr_date='$date' AND mode_transport='train'
              GROUP BY arr_trn, arr_stn
              ORDER BY arr_stn, arr_time;";
    $result = mysql_query($query, $con);

  ?>

    <b>Date: </b><?php echo date('d-m-Y', strtotime($date)); ?><br/><br/>
    <table width="80%" cellspacing="0" border="1">
      <tr>
        <th>S.No.</th>
        <th>Arrival Time</th>
        <th>Train No.</th>
        <th>Pickup Station</th>
        <th>Origin State</th>
        <th>No. of Participants</th>
      </tr>

    <?php

        for($j = 0; $j < mysql_num_rows($result); $j ++) {

          $time = mysql_result($result, $j, "arr_time");
          $train = mysql_result($result, $j, "arr_trn");
          $station = mysql_result($result, $j, "arr_stn");
          $state = mysql_result($result, $j, "state");
          $count = mysql_result($result, $j, "cnt");

          ?>

          <tr align="center">
            <td><?php echo $j + 1; ?></td>
            <td><?php echo date("g:i a", strtotime($time)); ?></td>
            <td><?php echo $train; ?></td>
            <td><?php echo $station; ?></td>
            <td><?php echo $state; ?></td>
            <td>
              <a href="train_info.php?arr_date=<?php echo $date; ?>&arr_trn=<?php echo $train; ?>&arr_stn=<?php echo $station; ?>" target="_blank">
                <?php echo $count; ?>
              </a>
            </td>
          </tr>

          <?php

        }

    ?>

  </table><br/><br/>

  <?php

  }

  mysql_close($con);

?>

</body>
</html>
