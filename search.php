<?php

	session_start();

	if(!isset($_SESSION['user']))
		header('location:login.php');

	include 'connection.php';

?>

<html>
<head>
<link rel="shortcut icon" href="http://cdn.artofliving.org/sites/all/themes/aol-zen/images/favicon.ico" type="image/vnd.microsoft.icon" />
<title>Search</title>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(function() {

});

function del(id, uid) {
	if(confirm("Are you sure?")) {
		$("#row" + id).hide();
		$.ajax({
				url: 'delete_entry.php',
				type: 'POST',
				data: 'id=' + uid,
				success: function(result) {
					alert('Successfully Deleted');
				}
		});
	}
	return false;

}
</script>
</head>

<body background="page_bg.jpg">

<h1>Search</h1>

<form method="post" action="search.php">

<input type="text" name="param">
<input type="submit" value="Search">

</form>

<br><br>

<?php

	if(isset($_POST['param'])) {

		?>

		<table cellspacing="0" width="100%" border="3">

			<tr align="center">
				<th>S.No.</th>
				<th>Generated ID</th>
				<th>Name</th>
				<th>Gender</th>
				<th>Age</th>
				<th>Father's/Husband's Name</th>
				<th>Institute</th>
				<th>ID Type</th>
				<th>ID No</th>
				<th>Mobile</th>
				<th>Email</th>
				<th>Address</th>
				<th>City</th>
				<th>Pincode</th>
				<th>State</th>
				<th>Instrument</th>
				<th>Qualification</th>
				<th>Experience</th>
				<th>Delete</th>
			</tr>

			<?php

				$param = $_POST['param'];

				$query = "SELECT * FROM users WHERE name LIKE '%$param%' OR fh_name LIKE '%$param%'
							OR Institute LIKE '%$param%' OR id_ref LIKE '%$param%' OR id_no LIKE '%$param%'
							OR mobile LIKE '%$param%' OR email LIKE '%$param%' OR address LIKE '%$param%'
							OR city LIKE '%$param%' OR u_id LIKE '%$param%';";

				$result = mysql_query($query, $con);

				for($i = 0; $i < mysql_num_rows($result); $i ++) {

					?>

					<tr align="center" id="row<?php echo $i + 1; ?>">
						<td><?php echo $i + 1; ?></td>
						<td><?php echo mysql_result($result, $i, "u_id"); ?></td>
						<td><?php echo mysql_result($result, $i, "name"); ?></td>
						<td><?php echo mysql_result($result, $i, "gender"); ?></td>
						<td><?php echo mysql_result($result, $i, "age"); ?></td>
						<td><?php echo mysql_result($result, $i, "fh_name"); ?></td>
						<td><?php echo mysql_result($result, $i, "institute"); ?></td>
						<td><?php echo mysql_result($result, $i, "id_ref"); ?></td>
						<td><?php echo mysql_result($result, $i, "id_no"); ?></td>
						<td><?php echo mysql_result($result, $i, "mobile"); ?></td>
						<td><?php echo mysql_result($result, $i, "email"); ?></td>
						<td><?php echo mysql_result($result, $i, "address"); ?></td>
						<td><?php echo mysql_result($result, $i, "city"); ?></td>
						<td><?php echo mysql_result($result, $i, "pincode"); ?></td>
						<td><?php echo mysql_result($result, $i, "state"); ?></td>
						<td><?php echo mysql_result($result, $i, "instrument"); ?></td>
						<td><?php echo mysql_result($result, $i, "qualification"); ?></td>
						<td><?php echo mysql_result($result, $i, "experience"); ?></td>
						<td><a href="#" onclick="del('<?php echo $i + 1; ?>', '<?php echo mysql_result($result, $i, "u_id"); ?>')">Delete</a></td>
					</tr>

					<?php

				}

			?>

		</table>

		<?php

	}

	?>

</body>

<?php

	mysql_close($con);

?>
